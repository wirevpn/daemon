# daemon

A *launchd* daemon for MacOS that manages WireGuard tunnels via *wireguard-go*.

* **network** is a simple socket server/client library to implement communication with the daemon.
* **server** is the actual daemon that listens to a single client at a time.
* **macos** has the files necessary to create an installable MacOS package.
* **windows** has the files necessary to build the Windows service