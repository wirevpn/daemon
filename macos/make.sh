#!/bin/bash

# build server
go build -ldflags="-X golang.zx2c4.com/wireguard/ipc.socketDirectory=/var/run/wirevpn/wireguard" ../server/
mkdir -p package/payload/usr/local/bin/
mv server package/payload/usr/local/bin/wirevpn-daemon

# build the package
chmod +x munkipkg
chmod +x package/scripts/*
./munkipkg package/