package main

import (
	"log"
	"os"
	"strings"
)

func init() {
	if len(os.Args) > 1 {
		cmd := strings.ToLower(os.Args[1])
		switch cmd {
		case "install":
			err := installService(svcName, svcDisplay)
			if err != nil {
				log.Fatalf("Error installing service: %v", err)
			}
		case "remove":
			err := removeService(svcName)
			if err != nil {
				log.Fatalf("Error removing service: %v", err)
			}
		case "stop":
			err := stopService(svcName)
			if err != nil {
				log.Fatalf("Error stopping service: %v", err)
			}
		}
		os.Exit(0)
	}
}
