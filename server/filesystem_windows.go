package main

import (
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"

	"github.com/hectane/go-acl"
	"golang.org/x/sys/windows"
)

var (
	dmnLogFile *os.File
	tnlLog     *log.Logger
	tnlLogErr  *log.Logger
	dmnLog     *log.Logger
	dmnLogErr  *log.Logger
)

func startLog() {
	dmnLogFile, err := os.OpenFile(logpath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("Error opening log file: %v", err)
	}
	dmnLog = log.New(dmnLogFile, "[DAEMON] ", 0)
	dmnLogErr = log.New(dmnLogFile, "[ERROR][DAEMON] ", 0)
	tnlLog = log.New(dmnLogFile, "[TUNNEL] ", 0)
	tnlLogErr = log.New(dmnLogFile, "[ERROR][TUNNEL] ", 0)
}

func closeLog() {
	dmnLogFile.Close()
}

func setupDirs() error {
	files, err := ioutil.ReadDir(maindir)
	if err != nil {
		err := os.MkdirAll(maindir, 0777)
		if err != nil {
			return err
		}
	} else {
		// First try to remove all files in maindir
		for _, f := range files {
			fname := f.Name()
			match, _ := regexp.MatchString("^wirevpn.*", fname)
			if match {
				os.RemoveAll(maindir + "\\" + fname)
			}
		}
	}

	// Then try to find a suitable one
	logfile := ""
	for i := 0; i < 100; i++ {
		logfile = "wirevpn.log." + strconv.Itoa(i)
		found := false
		for _, f := range files {
			if !f.IsDir() {
				if f.Name() == logfile {
					found = true
					break
				}
			}
		}
		if found {
			continue
		}
		break
	}

	logpath = maindir + "\\" + logfile

	return acl.Apply(
		maindir,
		false,
		false,
		acl.GrantName(windows.GENERIC_ALL, "Users"),
	)
}
