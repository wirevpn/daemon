package main

import "os"

const (
	svcName    = "WireVPN"
	svcDisplay = "WireVPN Service"
	intfname   = "wirevpn0"
)

var (
	maindir = os.Getenv("PROGRAMDATA") + "\\WireVPN"
	logpath = ""
)

func findInterface() (string, error) {
	return intfname, nil
}
