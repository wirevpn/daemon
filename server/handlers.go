package main

import "gitlab.com/wirevpn/daemon/network"

func handlers(t *tunnel) network.ServerHandlers {
	handlers := make(network.ServerHandlers)

	handlers[network.MsgConnect] = func(msg *network.Message) *network.Message {
		err := t.tunnelOn(msg.Data.(string))
		return &network.Message{
			ID:   network.MsgConnect,
			Data: err == nil,
		}
	}

	handlers[network.MsgDisconnect] = func(msg *network.Message) *network.Message {
		err := t.tunnelOff()
		return &network.Message{
			ID:   network.MsgDisconnect,
			Data: err == nil,
		}
	}

	return handlers
}
