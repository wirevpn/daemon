package main

import (
	"log"
	"os"
)

var (
	tnlLog    = log.New(os.Stdout, "[TUNNEL] ", 0)
	tnlLogErr = log.New(os.Stdout, "[ERROR][TUNNEL] ", 0)
	dmnLog    = log.New(os.Stdout, "[DAEMON] ", 0)
	dmnLogErr = log.New(os.Stdout, "[ERROR][DAEMON] ", 0)
)

func startLog() {}
func closeLog() {}

func setupDirs() error {
	err := os.RemoveAll(maindir)
	if err != nil {
		return err
	}
	return os.MkdirAll(utundir, 0755)
}
