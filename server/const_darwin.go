package main

import (
	"fmt"
	"net"
	"regexp"
	"strconv"
)

const (
	maindir  = "/var/run/wirevpn"
	utundir  = "/var/run/wirevpn/wireguard"
	sockfile = "wirevpn"
	quickBin = "/usr/local/bin/wirevpn-quick"
	dnsBin   = "networksetup"
)

var (
	intfname = "utun0"
)

func findInterface() (string, error) {
	intf, err := net.Interfaces()
	if err != nil {
		return "", err
	}

	r, err := regexp.Compile("\\Autun\\d\\z")
	if err != nil {
		return "", err
	}

	// List of used utun interfaces
	matches := make(map[string]bool)
	for _, v := range intf {
		match := r.FindString(v.Name)
		if match != "" {
			matches[v.Name] = true
		}
	}

	// Find the first unused one
	var i int
	for i = 0; i < 10; i++ {
		c := "utun" + strconv.Itoa(i)
		if _, ok := matches[c]; !ok {
			intfname = c
			break
		}
	}

	// No device left to use
	if i == 9 && intfname == "utun0" {
		return "", fmt.Errorf("No tun device left to use")
	}

	return intfname, nil
}
