/* SPDX-License-Identifier: MIT
 *
 * Copyright (C) 2017-2019 WireGuard LLC. All Rights Reserved.
 */

package main

import (
	"fmt"
	"syscall"

	"golang.org/x/sys/windows"
)

type errWin uint32

const (
	errorSuccess errWin = iota
	errorRingloggerOpen
	errorLoadConfiguration
	errorCreateWintun
	errorDetermineWintunName
	errorUAPIListen
	errorDNSLookup
	errorFirewall
	errorDeviceSetConfig
	errorBindSocketsToDefaultRoutes
	errorSetNetConfig
	errorDetermineExecutablePath
	errorCreateSecurityDescriptor
	errorOpenNULFile
	errorTrackTunnels
	errorEnumerateSessions
	errorWin32
	errorFlushInterface
)

func (e errWin) Error() string {
	switch e {
	case errorSuccess:
		return "No error"
	case errorRingloggerOpen:
		return "Unable to open log file"
	case errorDetermineExecutablePath:
		return "Unable to determine path of running executable"
	case errorLoadConfiguration:
		return "Unable to load configuration from path"
	case errorCreateWintun:
		return "Unable to create Wintun device"
	case errorDetermineWintunName:
		return "Unable to determine Wintun name"
	case errorUAPIListen:
		return "Unable to listen on named pipe"
	case errorDNSLookup:
		return "Unable to resolve one or more DNS hostname endpoints"
	case errorFirewall:
		return "Unable to enable firewall rules"
	case errorDeviceSetConfig:
		return "Unable to set device configuration"
	case errorBindSocketsToDefaultRoutes:
		return "Unable to bind sockets to default route"
	case errorSetNetConfig:
		return "Unable to set interface addresses, routes, dns, and/or adapter settings"
	case errorCreateSecurityDescriptor:
		return "Unable to determine security descriptor"
	case errorOpenNULFile:
		return "Unable to open NUL file"
	case errorTrackTunnels:
		return "Unable to track existing tunnels"
	case errorEnumerateSessions:
		return "Unable to enumerate current sessions"
	case errorWin32:
		return "An internal Windows error has occurred"
	case errorFlushInterface:
		return "Unable to flush interface"
	default:
		return "An unknown error has occurred"
	}
}

func determineErrorCode(err error, serviceError errWin) (bool, uint32) {
	if syserr, ok := err.(syscall.Errno); ok {
		return false, uint32(syserr)
	} else if serviceError != errorSuccess {
		return true, uint32(serviceError)
	} else {
		return false, windows.NO_ERROR
	}
}

func combineErrors(err error, serviceError errWin) error {
	if serviceError != errorSuccess {
		if err != nil {
			return fmt.Errorf("%v: %v", serviceError, err)
		}
		return serviceError
	}
	return err
}
