// +build darwin windows

package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/wirevpn/daemon/network"
)

func main() {
	var (
		connEstablished = make(chan bool, 1)
		connWait        = make(chan bool, 1)
		terminate       = make(chan os.Signal, 1)
	)

	// Cleanup main directory and setup directories
	if err := setupDirs(); err != nil {
		log.Printf("Error setting up directories: %v", err)
		os.Exit(1)
	}

	// Init logs
	startLog()
	defer closeLog()

	// Start the tunnel device
	tnl := newTunnel(tnlLogErr, tnlLog)
	go func() {
		dmnLog.Printf("Trying to bring tun device up")
		err := tnl.start()
		if err != nil {
			dmnLogErr.Fatalf("Tunnel device couldn't be started: %v", err)
		}
	}()
	select {
	case intf := <-tnl.started:
		dmnLog.Printf("Tun device %v is up", intf)
	case <-time.After(5 * time.Second):
		dmnLogErr.Fatalf("Can't bring tun device up")
	}

	// Start daemon
	server, err := network.NewServer(handlers(tnl))
	if err != nil {
		dmnLogErr.Fatalf("Can't create service: %v", err)
	}
	defer server.Close()
	tnl.setInterruptFn(server.Interrupt)

	dmnLog.Printf("Started")
	defer dmnLog.Printf("Stopped")

	// Need to gracefully exit since daemon can be killed by anyone
	signal.Notify(terminate, os.Interrupt, os.Kill, syscall.SIGTERM)

	// Kickstart state machine
	connWait <- true

	for {
		select {
		case <-connWait:
			go func() {
				dmnLog.Printf("Waiting for connection...")
				err := server.Accept()
				if err != nil {
					dmnLogErr.Printf("Connection error: %v", err)
					return
				}
				connEstablished <- true
			}()

		case <-connEstablished:
			go func() {
				dmnLog.Printf("Connection accepted")
				dmnLog.Printf("Waiting for requests...")

				// Blocks until client is disconnected
				server.Start()

				// Get tunnel down
				tnl.tunnelOff()

				// Back to listening
				connWait <- true
			}()

		case <-terminate:
			tnl.quit <- true
			dmnLog.Printf("Trying to bring tun device down")
			select {
			case intf := <-tnl.stopped:
				dmnLog.Printf("Tun device %v is down", intf)
			case <-time.After(20 * time.Second):
				dmnLogErr.Printf("Can't bring tun device down")
			}
			return
		}
	}
}
