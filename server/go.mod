module gitlab.com/wirevpn/daemon/server

go 1.12

require (
	gitlab.com/wirevpn/common/config v0.0.0-20190418205304-90234bbc7d90
	gitlab.com/wirevpn/daemon/network v0.0.0-20190410205940-e66b8092fb9d
	golang.org/x/crypto v0.0.0-20190418165655-df01cb2cc480 // indirect
	golang.org/x/net v0.0.0-20190420063019-afa5a82059c6 // indirect
	golang.org/x/sys v0.0.0-20190419153524-e8e3143a4f4a // indirect
	golang.zx2c4.com/wireguard v0.0.0-20190419080811-f1dc167901ae
)
