/* Most of the code here belongs to Jason A. Donenfeld et al. */
/* See https://git.zx2c4.com/wireguard-go/ */

package main

import (
	"bufio"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"gitlab.com/wirevpn/common/config"
	"golang.zx2c4.com/wireguard/device"
	"golang.zx2c4.com/wireguard/ipc"
	"golang.zx2c4.com/wireguard/tun"
)

type tunnel struct {
	logErr    *log.Logger
	logInfo   *log.Logger
	started   chan string
	stopped   chan string
	quit      chan bool
	intf      string
	alive     bool
	interrupt func()
	lock      sync.Mutex
	dev       *device.Device
}

func newTunnel(logErr, logInfo *log.Logger) *tunnel {
	return &tunnel{
		logErr:  logErr,
		logInfo: logInfo,
		started: make(chan string, 1),
		stopped: make(chan string, 1),
		quit:    make(chan bool, 1),
	}
}

func (t *tunnel) start() error {
	defer func() {
		if r := recover(); r != nil {
			t.logErr.Printf("Panic recovered in tunnel: %v", r)
		}
		tunsock := filepath.Join(utundir, t.intf, ".sock")
		os.Remove(tunsock)
		close(t.quit)
		close(t.started)
		close(t.stopped)
	}()

	interfaceName, err := findInterface()
	if err != nil {
		return err
	}

	tun, err := tun.CreateTUN(interfaceName, device.DefaultMTU)
	if err != nil {
		return err
	}

	realInterfaceName, err := tun.Name()
	if err != nil {
		return err
	}
	interfaceName = realInterfaceName
	t.intf = realInterfaceName

	wglogger := &device.Logger{
		Debug: t.logInfo,
		Info:  t.logInfo,
		Error: t.logErr,
	}
	dev := device.NewDevice(tun, wglogger)
	defer dev.Close()
	t.dev = dev

	uapiFile, err := ipc.UAPIOpen(interfaceName)
	if err != nil {
		return err
	}
	defer uapiFile.Close()

	uapi, err := ipc.UAPIListen(interfaceName, uapiFile)
	if err != nil {
		return err
	}
	defer uapi.Close()

	t.started <- interfaceName

	select {
	case <-dev.Wait():
		quickDown(interfaceName)
		t.interrupt()
	case <-t.quit:
		quickDown(interfaceName)
		t.stopped <- interfaceName
	}

	return nil
}

func (t *tunnel) tunnelOn(c string) error {
	if t.isAlive() {
		return nil
	}
	t.setAlive(true)

	cfg, err := config.OpenString(c)
	if err != nil {
		t.setAlive(false)
		return err
	}

	if err := t.dev.IpcSetOperation(bufio.NewReader(strings.NewReader(cfg.IPC()))); err != nil {
		t.setAlive(false)
		return err
	}

	err = quickUp(t.intf, cfg)
	if err != nil {
		t.setAlive(false)
		return err
	}

	t.logInfo.Printf("Tunnel turned on")

	return nil
}

func (t *tunnel) tunnelOff() error {
	if !t.isAlive() {
		return nil
	}
	t.setAlive(false)

	err := quickDown(t.intf)
	if err != nil {
		t.setAlive(true)
		return err
	}

	t.logInfo.Printf("Tunnel turned off")

	return nil
}

func (t *tunnel) isAlive() bool {
	t.lock.Lock()
	defer t.lock.Unlock()
	return t.alive
}

func (t *tunnel) setAlive(state bool) {
	t.lock.Lock()
	defer t.lock.Unlock()
	t.alive = state
}

func (t *tunnel) setInterruptFn(interrupt func()) {
	t.interrupt = interrupt
}
