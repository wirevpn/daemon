package main

import (
	"fmt"
	"os/exec"
	"strings"
	"sync"

	"gitlab.com/wirevpn/common/config"
)

var (
	services = make(map[string][]string)
	dnsLock  sync.Mutex
)

func toSh(arr []string) string {
	return strings.Join(arr, " ")
}

func quickUp(intf string, cfg *config.File) error {
	var (
		i *config.Interface
		p *config.Peer
	)

	if cfg.Interface != nil {
		i = cfg.Interface
	} else {
		return fmt.Errorf("Config file doesn't have an interface")
	}

	if len(cfg.Peers) >= 0 {
		p = cfg.Peers[0]
	} else {
		return fmt.Errorf("Config file doesn't have a peer")
	}

	// TODO: Err needs to be logged
	setDNS(i.DNS)

	ind := strings.LastIndex(p.Endpoint, ":")
	cmd := []string{"up", intf, toSh(i.Address), toSh(p.AllowedIP), p.Endpoint[:ind]}
	if err := exec.Command(quickBin, cmd...).Run(); err != nil {
		return fmt.Errorf("wirevpn-quick up: %v", err)
	}

	return nil
}

func quickDown(intf string) error {
	// TODO: Err needs to be logged
	restoreDNS()

	cmd := []string{"down", intf}
	if err := exec.Command(quickBin, cmd...).Run(); err != nil {
		return fmt.Errorf("wirevpn-quick down: %v", err)
	}
	return nil
}

func restoreDNS() error {
	dnsLock.Lock()
	defer dnsLock.Unlock()

	var errors string
	for k, v := range services {
		cmd := []string{"-setdnsservers", k}
		if len(v) == 0 {
			cmd = append(cmd, "Empty")
		} else {
			cmd = append(cmd, v...)
		}
		if err := exec.Command(dnsBin, cmd...).Run(); err != nil {
			errors += err.Error() + "\n"
		}
	}
	if errors != "" {
		return fmt.Errorf("%s", errors)
	}
	return nil
}

func setDNS(dns []string) error {
	dnsLock.Lock()
	defer dnsLock.Unlock()

	services = make(map[string][]string)

	out, err := exec.Command(dnsBin, "-listallnetworkservices").Output()
	if err != nil {
		return err
	}

	for _, v := range strings.Split(string(out), "\n") {
		v = strings.TrimSpace(v)
		if !strings.Contains(v, "*") && v != "" {
			// Get previous
			dnsList, err := exec.Command(dnsBin, "-getdnsservers", v).Output()
			if err != nil {
				continue
			}
			// Set new
			cmd := append([]string{"-setdnsservers", v}, dns...)
			o, err := exec.Command(dnsBin, cmd...).Output()
			if err == nil && !strings.Contains(string(o), "Error") {
				services[v] = []string{}
				for _, d := range strings.Split(string(dnsList), "\n") {
					d = strings.TrimSpace(d)
					if d != "" && !strings.Contains(d, "Empty") && !strings.Contains(d, "DNS") {
						services[v] = append(services[v], d)
					}
				}
			}
		}
	}

	return nil
}
