/* Most of the code here belongs to Jason A. Donenfeld et al. */
/* See https://git.zx2c4.com/wireguard-go/ */

package main

import (
	"bufio"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"gitlab.com/wirevpn/common/config"
	"golang.org/x/sys/windows/svc"
	"golang.org/x/sys/windows/svc/debug"
	"golang.zx2c4.com/winipcfg"
	"golang.zx2c4.com/wireguard/device"
	"golang.zx2c4.com/wireguard/ipc"
	"golang.zx2c4.com/wireguard/tun"
	"golang.zx2c4.com/wireguard/windows/service/firewall"
)

type tunnel struct {
	logErr    *log.Logger
	logInfo   *log.Logger
	noErr     chan error
	started   chan string
	stopped   chan string
	quit      chan bool
	intf      string
	alive     bool
	interrupt func()
	lock      sync.Mutex
	elog      debug.Log
	dev       *device.Device
	rc        *winipcfg.RouteChangeCallback
	tun       *tun.NativeTun
}

func newTunnel(logErr, logInfo *log.Logger) *tunnel {
	return &tunnel{
		logErr:  logErr,
		logInfo: logInfo,
		noErr:   make(chan error, 1),
		started: make(chan string, 1),
		stopped: make(chan string, 1),
		quit:    make(chan bool, 1),
	}
}

func (t *tunnel) Execute(args []string, r <-chan svc.ChangeRequest, changes chan<- svc.Status) (ssec bool, errno uint32) {
	changes <- svc.Status{State: svc.StartPending}

	var (
		err          error
		wglogger     *device.Logger
		serviceError = errorSuccess
	)

	defer func() {
		if r := recover(); r != nil {
			t.logErr.Printf("Panic recovered in tunnel: %v", r)
		}
		changes <- svc.Status{State: svc.StopPending}
		if err != nil {
			t.noErr <- err
		}
		ssec, errno = determineErrorCode(err, serviceError)
		close(t.quit)
		close(t.started)
		close(t.stopped)
	}()

	wintun, err := tun.CreateTUN(intfname)
	if err != nil {
		serviceError = errorCreateWintun
		return
	}

	realInterfaceName, err := wintun.Name()
	if err != nil || realInterfaceName == "" {
		serviceError = errorDetermineWintunName
		return
	}
	t.intf = realInterfaceName
	t.tun = wintun.(*tun.NativeTun)

	// TODO: Release version shouldn't be so chatty
	wglogger = &device.Logger{
		Debug: t.logInfo,
		Info:  t.logInfo,
		Error: t.logErr,
	}
	dev := device.NewDevice(t.tun, wglogger)
	defer dev.Close()
	t.dev = dev

	uapi, err := ipc.UAPIListen(t.intf)
	if err != nil {
		serviceError = errorUAPIListen
		return
	}
	defer uapi.Close()

	t.dev.Up()
	t.tun.Flush()

	changes <- svc.Status{State: svc.Running, Accepts: svc.AcceptStop}
	t.noErr <- nil
	t.started <- t.intf

	for {
		select {
		case c := <-r:
			switch c.Cmd {
			case svc.Stop:
				return
			case svc.Interrogate:
				changes <- c.CurrentStatus
			}
		case <-dev.Wait():
			t.winDown()
			t.interrupt()
		case <-t.quit:
			t.winDown()
			t.stopped <- t.intf
		}
	}
}

func (t *tunnel) start() error {
	if err := svc.Run(svcName, t); err != nil {
		return err
	}
	select {
	case err := <-t.noErr:
		return err
	case <-time.After(20 * time.Second):
		return fmt.Errorf("Timed out")
	}
}

func (t *tunnel) tunnelOn(c string) error {
	if t.isAlive() {
		return nil
	}
	t.setAlive(true)

	cfg, err := config.OpenString(c)
	if err != nil {
		t.setAlive(false)
		return err
	}

	if err := t.dev.IpcSetOperation(bufio.NewReader(strings.NewReader(cfg.IPC()))); err != nil {
		t.setAlive(false)
		return err
	}

	err = t.winUp(cfg)
	if err != nil {
		t.setAlive(false)
		return err
	}

	time.Sleep(5 * time.Second)

	t.logInfo.Printf("Tunnel turned on")
	return nil
}

func (t *tunnel) tunnelOff() error {
	if !t.isAlive() {
		return nil
	}
	t.setAlive(false)

	err := t.winDown()
	if err != nil {
		t.setAlive(true)
		return err
	}

	time.Sleep(5 * time.Second)

	t.logInfo.Printf("Tunnel turned off")

	return nil
}

func (t *tunnel) isAlive() bool {
	t.lock.Lock()
	defer t.lock.Unlock()
	return t.alive
}

func (t *tunnel) setAlive(state bool) {
	t.lock.Lock()
	defer t.lock.Unlock()
	t.alive = state
}

func (t *tunnel) setInterruptFn(interrupt func()) {
	t.interrupt = interrupt
}

func (t *tunnel) winUp(cfg *config.File) error {
	err := enableFirewall(cfg, t.tun)
	if err != nil {
		return err
	}

	t.rc, err = monitorDefaultRoutes(t.dev, cfg.Interface.MTU == 0, t.tun)
	if err != nil {
		return err
	}

	err = configureInterface(cfg, t.tun)
	if err != nil {
		return err
	}
	return nil
}

func (t *tunnel) winDown() error {
	firewall.DisableFirewall()
	if t.rc != nil {
		if err := t.rc.Unregister(); err != nil {
			return err
		}
	}
	return flushInterface(t.tun)
}
