package network

import (
	"encoding/gob"
	"io"
	"net"
	"sync"
)

// ClientHandlers holds the response callbacks
type ClientHandlers map[int](func(msg *Message))

// Client holds connection information to daemon
type Client struct {
	conn     net.Conn
	decoder  *gob.Decoder
	encoder  *gob.Encoder
	handlers ClientHandlers
	online   bool
	lock     sync.Mutex
}

// NewClient creates a new client with the given unix socket and response callbacks
func NewClient(handlers ClientHandlers) (*Client, error) {
	return &Client{
		handlers: handlers,
	}, nil
}

// Establish establishes the connection to daemon
func (c *Client) Establish() error {
	conn, err := net.Dial("tcp", "localhost:55661")
	if err != nil {
		if f, ok := c.handlers[ConnError]; ok {
			f(&Message{
				ID:   ConnError,
				Data: err,
			})
		}
		return err
	}
	c.conn = conn
	c.decoder = gob.NewDecoder(conn)
	c.encoder = gob.NewEncoder(conn)
	c.setOnline(true)
	return nil
}

// Connect sends a MsgConnect request to daemon
func (c *Client) Connect(config string) {
	if c.isOnline() {
		msg := &Message{
			ID:   MsgConnect,
			Data: config,
		}
		c.encoder.Encode(msg)
	}
}

// Disconnect sends a MsgDisconncet request to daemon
func (c *Client) Disconnect() {
	if c.isOnline() {
		msg := &Message{
			ID:   MsgDisconnect,
			Data: nil,
		}
		c.encoder.Encode(msg)
	}
}

// Start starts the main loop
func (c *Client) Start() {
	defer func() {
		c.Close()
		if f, ok := c.handlers[ConnClosed]; ok {
			f(&Message{
				ID: ConnClosed,
			})
		}
	}()
	for {
		var msg Message
		if err := c.decoder.Decode(&msg); err != nil {
			if err == io.EOF {
				return
			}
			continue
		}
		if f, ok := c.handlers[msg.ID]; ok {
			f(&msg)
		}
	}
}

// Close does cleanup
func (c *Client) Close() {
	if c.isOnline() {
		c.conn.Close()
		c.online = false
	}
}

func (c *Client) isOnline() bool {
	c.lock.Lock()
	defer c.lock.Unlock()
	return c.online
}

func (c *Client) setOnline(state bool) {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.online = state
}
