package network

import (
	"encoding/gob"
	"io"
	"net"
	"sync"
)

// ServerHandlers holds the request/response callbacks
type ServerHandlers map[int](func(msg *Message) *Message)

// Server holds connection information to client
type Server struct {
	listener net.Listener
	conn     net.Conn
	decoder  *gob.Decoder
	encoder  *gob.Encoder
	handlers ServerHandlers
	online   bool
	lock     sync.Mutex
}

// NewServer creates a new daemon with the given unix socket and request/response callbacks
func NewServer(handlers ServerHandlers) (*Server, error) {
	ln, err := net.Listen("tcp", "localhost:55661")
	if err != nil {
		return nil, err
	}
	return &Server{
		listener: ln,
		handlers: handlers,
	}, nil
}

// Accept starts a connection with a client
func (s *Server) Accept() error {
	conn, err := s.listener.Accept()
	if err != nil {
		return err
	}
	s.conn = conn
	s.decoder = gob.NewDecoder(conn)
	s.encoder = gob.NewEncoder(conn)
	s.setOnline(true)
	return nil
}

// Start starts the main loop
func (s *Server) Start() {
	defer func() {
		s.conn.Close()
		s.setOnline(false)
	}()
	for {
		var msg Message
		if err := s.decoder.Decode(&msg); err != nil {
			if _, ok := err.(net.Error); ok || err == io.EOF {
				return
			}
			continue
		}
		if f, ok := s.handlers[msg.ID]; ok {
			s.encoder.Encode(f(&msg))
		} else {
			// If the message is unknown close the connection
			return
		}
	}
}

// Interrupt sends MsgInterrupt event to the client
func (s *Server) Interrupt() {
	if s.isOnline() {
		msg := &Message{
			ID:   MsgInterrupt,
			Data: true,
		}
		s.encoder.Encode(msg)
	}
}

// Close does cleanup
func (s *Server) Close() {
	if s.isOnline() {
		s.conn.Close()
		s.setOnline(false)
	}
	s.listener.Close()
}

func (s *Server) isOnline() bool {
	s.lock.Lock()
	defer s.lock.Unlock()
	return s.online
}

func (s *Server) setOnline(state bool) {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.online = state
}
