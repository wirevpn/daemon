package network

const (
	// MsgConnect message id for tunnel connection request/response
	MsgConnect = iota

	// MsgDisconnect message id for tunnel disconnection request/response
	MsgDisconnect

	// MsgInterrupt message id for tunnel connection interruption event
	MsgInterrupt

	// ConnClosed message id for when the socket connection is closed
	ConnClosed

	// ConnError message id for when the socket connection can't be established
	ConnError
)

// Message holds data of the messages sent/received between daemon and client
type Message struct {
	ID   int
	Data interface{}
}
